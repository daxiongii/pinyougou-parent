package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.sellergoods.service.BrandService;
import entity.PageResult;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/brand")
public class BrandController {
		@Reference
		private BrandService brandService;
		/**
		 * 返回全部列表
		 * 
		 */
		@RequestMapping("/findAll")
		public List<TbBrand> findAll(){
			return brandService.findAll();
		}
		/**
		 * 返回分页结果数据
		 */
		@RequestMapping("/findPage")
		public PageResult findPage(int page,int rows) {
			return brandService.findPage(page,rows );
		}
		@RequestMapping("/add")
		public Result addRand(@RequestBody TbBrand tbBrand) {
			try {
				brandService.add(tbBrand);
				return new Result(true,"添加成功");
			} catch (Exception e) {
				
				return new Result(false,"添加失败");
			}
		}
		@RequestMapping("/findOne")
		public TbBrand findOne(Long id) {
			return brandService.findOne(id);
		}
		@RequestMapping("/update")
		public Result update(@RequestBody TbBrand tbBrand) {
				try {
					brandService.update(tbBrand);
					return new Result(true,"更改成功");
				} catch (Exception e) {
					
					return new Result(false,"更改失败");
				}
		}
		@RequestMapping("/delete")
		public Result delete(Long[] ids) {
			try {
				brandService.delete(ids);
				return new Result(true,"更改成功");
			} catch (Exception e) {
				
				return new Result(false,"更改失败");
			}
		}
		
		@RequestMapping("/search")
		public PageResult search(@RequestBody TbBrand tbBrand,int page,int rows) {
			return brandService.findPage(tbBrand, page, rows);
		}
		@RequestMapping("/selectOptionList")
		public List<Map> selectOptionList(){
			List<Map> selectOptionList = brandService.selectOptionList();
			return selectOptionList;
		}
}
