 //控制层
app.controller('goodsController' ,function($scope,$controller,itemCatService,goodsService){

	$controller('baseController',{$scope:$scope});//继承
    $scope.status=['未审核','已审核','审核未通过','关闭'];//商品状态
    $scope.itemCatList=[];//商品分类列表
    //加载商品分类列表
    $scope.findItemCatList=function(){
        itemCatService.findAll().success(
            function (reponse) {
                for (var i = 0; i < reponse.length; i++) {
                    //特定格式id作为索引值,value作为值
                    $scope.itemCatList[reponse[i].id]=reponse[i].name;
                }
            }
        )
    }
    $scope.updateStatus=function(status){
    	goodsService.updateStatus( $scope.selectIds,status).success(
    		function (response) {
                if(response.success){
                    $scope.reloadList();//刷新列表
                    $scope.selectIds=[];//清空ID集合
                    //提示成功
                    alert('修改成功');
                }else{
                    alert(response.message);
                }
            }

		)
	}
    //读取列表数据绑定到表单中
	$scope.findAll=function(){
		goodsService.findAll().success(
			function(response){
				$scope.list=response;
			}
		);
	}

	//分页
	$scope.findPage=function(page,rows){
		goodsService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}
		);
	}

	//查询实体
	$scope.findOne=function(id){
		goodsService.findOne(id).success(
			function(response){
				$scope.entity= response;
			}
		);
	}

	//保存
	$scope.save=function(){
		var serviceObject;//服务层对象
        $scope.entity.goodsDesc.introduction=editor.html();
		if($scope.entity.goods.id!=null){//如果有IDs
			serviceObject=goodsService.update( $scope.entity ); //修改
		}else{
			serviceObject=goodsService.add( $scope.entity  );//增加
		}
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询
		        	$scope.reloadList();//重新加载
				}else{
					alert(response.message);
				}
			}
		);
	}


	//批量删除
	$scope.dele=function(){
		//获取选中的复选框
		goodsService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
				}
			}
		);
	}

	$scope.searchEntity={};//定义搜索对象

	//搜索
	$scope.search=function(page,rows){
		goodsService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}
		);
	}

    //添加
    $scope.add=function(){
       goodsService.add( $scope.entity) .success(
            function(response){
                if(response.success){
                    //提示成功
                    alert('保存成功');
                    //将数据清空
                    $scope.entity={};
                }else{
                    alert(response.message);
                }
            }
        );
    }


});
