package com.pinyougou.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.pinyougou.pojo.TbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.List;
import java.util.Map;

@Service
public class ItemSearchLister implements MessageListener {
    @Autowired
    private ItemSearchServiceImpl itemSearchService;
    @Override
    public void onMessage(Message message) {
        System.out.println("接受到消息");
        try {
            TextMessage textMessage = (TextMessage) message;
            List<TbItem> list =(List<TbItem> ) JSON.parseArray(textMessage.getText(),TbItem.class);
            for (TbItem item : list) {
                System.out.println(item.getId()+" "+item.getTitle());
                Map specMap = JSON.parseObject(item.getSpec(),Map.class);
                item.setSpecMap(specMap);
            }
            itemSearchService.importList(list);
            System.out.println("成功导入到索引库");
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
