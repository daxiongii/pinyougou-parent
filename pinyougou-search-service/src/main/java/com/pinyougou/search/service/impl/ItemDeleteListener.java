package com.pinyougou.search.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.Arrays;

@Service
public class ItemDeleteListener implements MessageListener {

    @Autowired
    private ItemSearchServiceImpl itemSearchService;
    @Override
    public void onMessage(Message message) {
        System.out.println("接受到消息");
        ObjectMessage objectMessage = (ObjectMessage) message;
        try {
            Long [] goodsIds = (Long[]) objectMessage.getObject();
            System.out.println("ItemDeleteListener监听接收到消息..."+goodsIds);
            itemSearchService.deleteByGoodsIds(Arrays.asList(goodsIds));
            System.out.println("成功删除索引库索引数据");
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
