package com.pinyougou.search.service;

import java.util.List;
import java.util.Map;

public interface ItemSearchService {

    /**
     * 导入数据 为了保证只有一个service和solr交互,所以这个服务写在searchService中
     * @param list
     */
    public void importList(List list);

    /**
     * 删除数据
     * @param goodsIdList 商品id列表
     */
    public void deleteByGoodsIds(List goodsIdList);
    /**
     * 搜索
     * @param searchMap
     * @return
     */
    public Map<String,Object> search (Map searchMap);
}
