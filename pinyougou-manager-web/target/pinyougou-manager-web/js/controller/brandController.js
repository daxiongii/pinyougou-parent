//定义控制器
app.controller('brandController', function($scope, $controller, brandService) {

	$controller('baseController', {$scope : $scope});// 继承baseController;

	// 获取品牌列表
	$scope.findAll = function() {
		// http://localhost:9101/admin/brand.html
		brandService.findAll().success(function(response) {
			$scope.list = response;
		});
	}

	// 分页查询
	$scope.findPage = function(page, rows) {
		brandService.findPage(page, rows).success(function(response) {
			$scope.list = response.rows;
			$scope.paginationConf.totalItems = response.total;
		});

	}

	// $scope.entity={};//品牌的对象

	// 添加
	$scope.add = function() {
		brandService.add($scope.entity).success(function(response) {
			if (response.success) {
				$scope.reloadList();
			} else {
				alert(response.message);
			}
		});
	}

	// 修改操作
	$scope.findOne = function(id) {
		brandService.findOne(id).success(function(response) {
			$scope.entity = response;
		});
	}
	// 保存
	$scope.save = function() {
		var methodObject;
		if ($scope.entity.id != null) {
			methodObject = brandService.update($scope.entity);
		} else {
			methodObject = brandService.add($scope.entity);
		}
		methodObject.success(function(response) {
			if (response.success) {
				$scope.reloadList();
			} else {
				alert(response.message);
			}
		});
	}

	// 删除操作

	$scope.dele = function() {
		brandService.dele($scope.selectIds).success(function(response) {
			if (response.success) {
				$scope.reloadList();
			} else {
				alert(response.message);
			}
		});
	}
	// 检索
	$scope.searchEntity = {};

	$scope.search = function(page, rows) {
		brandService.search(page, rows, $scope.searchEntity).success(
				function(response) {
					$scope.list = response.rows;
					$scope.paginationConf.totalItems = response.total;
				});
	}

});