//定义控制器
app.controller('loginController', function($scope, $controller, loginService) {

    $controller('baseController', {$scope : $scope});// 继承baseController;

    //获取登录姓名
    $scope.showLoginName = function() {
        // http://localhost:9101/admin/brand.html
        loginService.loginName().success(function(response) {
            $scope.loginName = response.loginName;
        });
    }


});