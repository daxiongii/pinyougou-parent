package com.pinyougou.sellergoods.service;
import com.pinyougou.pojo.TbBrand;
import entity.PageResult;

import java.util.List;
import java.util.Map;

/**
 * 品牌服务层接口
 */
public interface BrandService {

	
	/**
	 * 返回全部列表
	 * 
	 */
	public List<TbBrand> findAll();
	
	/**
	 * 返回分页结果
	 * 
	 */
	public PageResult findPage(int pageNum, int pageSize);
	/**
	 * 增加品牌
	 */
	public void add(TbBrand tbBrand);
	/**
	 * 修改品牌
	 */
	public void update(TbBrand tbBrand);
	/**
	 * 查询品牌信息
	 */
	public TbBrand findOne(Long id);
	/**
	 * 删除品牌信息
	 */
	public void delete(Long[] ids);
	/**
	 * 品牌查询
	 */
	public PageResult findPage(TbBrand tbBrand, int pageNum, int pageSize);
	
	/**
	 * 品牌下来框数据
	 */
	public List<Map> selectOptionList();
}
