//购物车控制层
app.controller('cartController',function ($scope,cartService) {


    //保存订单
    $scope.submitOrder=function(){
        $scope.order.receiverAreaName=$scope.address.address;//地址
        $scope.order.receiverMobile=$scope.address.mobile;//手机
        $scope.order.receiver=$scope.address.contact;//联系人
        cartService.submitOrder($scope.order).success(
            function (response) {
                if (response.success){
                    if($scope.order.paymentType == "1"){ //如果是微信支付,跳转到支付界面
                        location.href="pay.html";
                    }else {
                        location.href="paysuccess.html";
                    }

                } else {
                    alert(response.message);
                }

            }
        )

    }

    //默认支付方式为在线支付
    $scope.order={paymentType:'1'};
    //选择支付方式
    $scope.selectPayType=function(type){
        $scope.order.paymentType=type;
    }
    //选择地址
    $scope.selectAddress=function(address){
        $scope.address=address;
    }

    //判断是否是当前选中地址
    $scope.isSelectedAddress=function(address){
        if(address==$scope.address){
            return true;
        }else{
            return false;
        }
    }

    //获取地址列表
    $scope.findAddressList = function(){
        cartService.findAddressList().success(
            function (response) {
                $scope.addressList = response;
                for(var i=0 ; i<$scope.addressList.length;i++){
                    if($scope.addressList[i].isDefault=='1'){
                        $scope.address=$scope.addressList[i];
                        break;
                    }
                }
            }
        )
    }
    //查询购物车列表
    $scope.findCartList = function () {
        cartService.findCartList().success(
            function (response) {
                $scope.cartList=response;
                $scope.totalValue=cartService.sum($scope.cartList);
            }
        )
    }

    $scope.addGoodsToCartList=function (itemId,num) {
        cartService.addGoodsToCartList(itemId,num).success(
            function (response) {
                if (response.success) {
                    $scope.findCartList();//舒心列表
                }else {
                    alert(response.message);
                }

            }
        )
    }

})