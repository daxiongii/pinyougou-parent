 //控制层 
app.controller('userController' ,function($scope  ,userService){	
	
	
    //读取列表数据绑定到表单中  
	$scope.add=function(){
		
		//判断两次密码是否一致，如果不一致，返回
		if($scope.entity.password != $scope.password){
			alert("两次密码不一致！");
			return;
		}
		
		userService.add($scope.entity,$scope.code).success(
			function(response){
				alert(response.message);
			}			
		);
	}   
	
	//发送短信验证码的方法
	$scope.sendSms=function(){
		//必须输入手机号、手机号是否合法
		userService.sendSms($scope.entity.phone).success(
			function(response){
				alert(response.message);
			}
		);
	}
    
});	
