app.controller('baseController',function($scope){
	
	//分页控件配置  1 当前页 2 总记录数 3 每页显示条数 4.每页条数选项   onChange 四个属性值发生变化时，会触发方法
	$scope.paginationConf = {
			 currentPage: 1,
			 totalItems: 0,
			 itemsPerPage: 10,
			 perPageOptions: [10, 20, 30, 40, 50],
			 onChange: function(){
				 $scope.reloadList();
			 }
	}; 
	//刷新页面
	$scope.reloadList=function(){
		$scope.search($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage);
	}
	//复选列表数据格式初始化
	$scope.selectIds = [ ];

	//复选框数据添加
	$scope.updateSelection = function ($event,id){
		if($event.target.checked){
			$scope.selectIds.push(id);
		}else{
			var index = $scope.selectIds.indexOf(id);
			$scope.selectIds.splice(index,1)
		}
	}
	//json数据转成String类型
	$scope.jsonToString=function(jsonString,key){
		var json=JSON.parse(jsonString);//将json字符串转换为json对象
		var value="";
		for(var i=0;i<json.length;i++){		
			if(i>0){
				value+=","
			}
			value+=json[i][key];			
		}
		return value;
	}
	//从集合中按照key查询对象
	$scope.searchObjectByKey= function (list,key,keyValue) {
        for (var i = 0; i < list.length; i++) {
			if(list[i][key]==keyValue){
				return list[i];
			}
        }
        return null;
    }

	
})