 //控制层 
app.controller('goodsController' ,function($scope,$controller,$location,uploadService ,goodsService,itemCatService,typeTemplateService){
	
	$controller('baseController',{$scope:$scope});//继承


    $scope.image_entity={};//图片上传对象
    $scope.searchEntity={};//定义搜索对象
	$scope.entity={goods:{},goodsDesc:{itemImages:[],specificationItems:[]}};//entity初始化
    $scope.status=['未审核','已审核','审核未通过','关闭'];//商品状态
	$scope.itemCatList=[];//商品分类列表
	//加载商品分类列表
	$scope.findItemCatList=function(){
		itemCatService.findAll().success(
			function (reponse) {
                for (var i = 0; i < reponse.length; i++) {
                	//特定格式id作为索引值,value作为值
					$scope.itemCatList[reponse[i].id]=reponse[i].name;
                }
            }
		)
	}
	//创建sku列表
	$scope.createItemList=function(){
		$scope.entity.itemList=[{spec:{},price:0,num:99999,status:'0',isDefault:'0' } ];//初始
		var items = $scope.entity.goodsDesc.specificationItems;
        for (var i = 0; i < items.length; i++) {
			$scope.entity.itemList=
				addColumn($scope.entity.itemList, items[i].attributeName,items[i].attributeValue )
        }
	}
	//添加列值
    addColumn=function(list,columnName,conlumnValues){
		var newList=[];//新的集合
        for (var i = 0; i<list.length  ; i++) {
			var oldRow = list[i];
            for (var j = 0; j <conlumnValues.length ; j++) {
				var newRow = JSON.parse(JSON.stringify(oldRow));//深克隆
				newRow.spec[columnName]=conlumnValues[j];
				newList.push(newRow);
            }
        }
        return newList;
	}
	//格式列表多选处理
	$scope.updateSpecAttribute=function($event,name,value){
		var object=$scope.searchObjectByKey(
			$scope.entity.goodsDesc.specificationItems,'attributeName',name);
		if(object!=null){
			if($event.target.checked){
				object.attributeValue.push(value);
			}else{
				// 移除选项
				object.attributeValue.splice(object.attributeValue.indexOf(value),1);
				if(object.attributeValue.length==0){
					$scope.entity.goodsDesc.specificationItems.splice(
						$scope.entity.goodsDesc.specificationItems.indexOf(object),1
					);
				}
			}
		}else{
			$scope.entity.goodsDesc.specificationItems.push(
				{"attributeName":name,"attributeValue":[value]}
			)
		}
	}

	//读取一级分类
	$scope.selectItemCat1List=function(){
		itemCatService.findByparentId(0).success(
			function (reponse) {
				$scope.itemCat1List=reponse;
            }
		)
	}
	//读取二级分类
	$scope.$watch('entity.goods.category1Id',function (newValue,oldValue) {
		itemCatService.findByparentId(newValue).success(
			function (reponse) {
                $scope.itemCat2List=reponse;
                $scope.itemCat3List=[];
                $scope.entity.goods.typeTemplateId=null;

            }
		)
    })
    //读取三级分类
    $scope.$watch('entity.goods.category2Id',function (newValue,oldValue) {
        itemCatService.findByparentId(newValue).success(
            function (reponse) {
                $scope.itemCat3List=reponse;
                $scope.entity.goods.typeTemplateId=null;
            }
        )
    })
    //更改模板编码
    $scope.$watch('entity.goods.category3Id',function (newValue,oldValue) {
        itemCatService.findOne(newValue).success(
            function (reponse) {
                $scope.entity.goods.typeTemplateId=reponse.typeId;
            }
        );


    })
	//根据模块id选择品牌列表
    $scope.$watch('entity.goods.typeTemplateId',function (newValue,oldValue) {
        typeTemplateService.findOne(newValue).success(
            function (reponse) {
                $scope.typeTemplate=reponse;
                $scope.typeTemplate.brandIds=JSON.parse($scope.typeTemplate.brandIds)
            }
        );
        //查询规格列表
        typeTemplateService.findSpecList(newValue).success(
            function(response){
                $scope.specList=response;
            }
        );
    })
    //添加图片列表
    $scope.add_image_entity=function(){
        $scope.entity.goodsDesc.itemImages.push($scope.image_entity);
    }
    //删除图片列表
    $scope.remove_image_entity=function(index){
        $scope.entity.goodsDesc.itemImages.splice(index,1);
    }

  //上传图片
    $scope.uploadFile=function(){
        uploadService.uploadFile().success(
			function (response) {
                if(response.success){
                    $scope.image_entity.url=response.message;
                }else{
                    alert(response.message);
                }
            }
		);
	}
    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
		goodsService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}

	//分页
	$scope.findPage=function(page,rows){			
		goodsService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(){
		var id = $location.search()['id'];//获取get请求参数值
		if(id==null){
			return;
		}
		goodsService.findOne(id).success(
			function(response){
				$scope.entity= response;
				//想富文本编辑器添加商品介绍
				editor.html($scope.entity.goodsDesc.introduction);
				//显示图片列表
				$scope.entity.goodsDesc.itemImages=JSON.parse($scope.entity.goodsDesc.itemImages);
				//规格数据
				$scope.entity.goodsDesc.specificationItems=JSON.parse($scope.entity.goodsDesc.specificationItems);
				//规格列表数据
                for (var i = 0; i <$scope.entity.itemList.length ; i++) {
                    $scope.entity.itemList[i].spec = JSON.parse($scope.entity.itemList[i].spec);
                }
			}
		);				
	}
	$scope.checkAttributeValue=function(name,values){
		var items = $scope.entity.goodsDesc.specificationItems;
		var object= $scope.searchObjectByKey(items,'attributeName',name);
		// 判断该行是否有被选中的项
		if(object!=null){
			//判断该行中的该列是否有值
			if(object.attributeValue.indexOf(values)>=0){
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}
	//保存 
	$scope.save=function(){
		var serviceObject;//服务层对象
        $scope.entity.goodsDesc.introduction=editor.html();
		if($scope.entity.goods.id!=null){//如果有ID
			serviceObject=goodsService.update( $scope.entity ); //修改  
		}else{
			serviceObject=goodsService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
                    // $scope.reloadList();//重新加载
                    location.href="goods.html";
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		goodsService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
				}						
			}		
		);				
	}
	

	
	//搜索
	$scope.search=function(page,rows){
		goodsService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
    //添加
    $scope.add=function(){
        $scope.entity.goodsDesc.introduction=editor.html();
        goodsService.add( $scope.entity) .success(
            function(response){
                if(response.success){
                    //提示成功
                    alert('保存成功');
                    //将数据清空
                    $scope.entity={};
                    editor.html('');
                }else{
                    alert(response.message);
                }
            }
        );
    }
    
});	
