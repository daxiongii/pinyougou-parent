# 学习案例
## **品优购**
   * 准备工作
   
        * 安装dubbox ,zookeeper 
              
            1. 解决耦合问题:为service层和web层通讯提供服务
            2. zookeeper 为dubbox服务
            3. zookeeper ip:zookeeper://192.168.25.128:2181
            4. service端 服务的接口为:20881

        * 导入数据库(otherFile文件夹下)
            1. 导入到本地数据库
            2. 代码里直接配的是localhost
           
        * 使用逆向工程生成的pojo和dao模块下的代码
        
        * 使用代码生成器(仅适用于简单的工程)
            1. 生成的js代码中controller层和service层打码
            2. 生成web端controller层代码,
            3. 生成service端service层代码
           
        *  使用maven插件来启动tomcat(web端的端口都是91XX,service端的端口都是90XX)
            1. service的服务端口:9001
            2. web的服务端口    :9101

        *  使用前先install 父工程
            1. maven工程,需要将pojo,dao,interface层代码install到本地仓库
            2. 单个install可能会报错,直接install整个工程没问题(很蛋疼)


## ***目前进度***

    1.品牌管理完成
        * 认识电商
        * dubbox入门
        * 系统框架介绍
        * angular js 入门
        * 分页实现 
            > 后端MyBatis分页插件:PageHelper 
            > 前端angular js模块:pagination
    2.规格管理完成
        * 前端js代码分层抽取
        * select2 实现多选下拉(深入理解json数据格式)
    3.模板管理完成
    4.Spring Security 入门程序完成  
    > 第五天 
    1.完善登录权限控制,
    2.完成商家注册及审批 
    3.注册用户密码加密(BCrypt加密机制)