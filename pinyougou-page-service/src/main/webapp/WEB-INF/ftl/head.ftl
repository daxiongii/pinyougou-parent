<div class="crumb-wrap">
    <ul class="sui-breadcrumb">
        <li>
            <a href="#">${itemCat1}</a>
        </li>
        <li>
            <a href="#">${itemCat2}</a>
        </li>
        <li>
            <a href="#">${itemCat3}</a>
        </li>
        <#--<li class="active">iphone 6S系类</li>-->
    </ul>
</div>
			<!--product-info-->
			<div class="product-info">
                <div class="fl preview-wrap">
                    <!--放大镜效果-->
                    <div class="zoom">
                        <!--默认第一个预览-->
                        <div id="preview" class="spec-preview">
                            <#assign imageList=goodsDesc.itemImages?eval />
                            <span class="jqzoom">
                                <#if imageList?size gt 0 >
                                <img jqimg="${imageList[0].url}" src="${imageList[0].url}" width="400px" height="400px" />
                                </#if>
                            </span>
                        </div>
                        <!--下方的缩略图-->
                        <div class="spec-scroll">
                            <a class="prev">&lt;</a>
                            <!--左右按钮-->
                            <div class="items">
                                <ul>
                                    <#list imageList as item>
                                    <li><img src="${item.url}" bimg="${item.url}" onmousemove="preview(this)" /></li>
                                    </#list>
                                </ul>
                            </div>
                            <a class="next">&gt;</a>
                        </div>
                    </div>
                </div>
                <div class="fr itemInfo-wrap">
                    <div class="sku-name">
                        <h4>{{sku.title}}</h4>
                    </div>
                    <div class="news"><span>${goods.caption}</span></div>
                    <div class="summary">
                        <div class="summary-wrap">
                            <div class="fl title">
                                <i>价　　格</i>
                            </div>
                            <div class="fl price">
                                <i>¥</i>
                                <em>{{sku.price}}</em>
                                <span>降价通知</span>
                            </div>
                            <div class="fr remark">
                                <i>累计评价</i><em>612188</em>
                            </div>
                        </div>
                        <div class="summary-wrap">
                            <div class="fl title">
                                <i>促　　销</i>
                            </div>
                            <div class="fl fix-width">
                                <i class="red-bg">加价购</i>
                                <em class="t-gray">满999.00另加20.00元，或满1999.00另加30.00元，或满2999.00另加40.00元，即可在购物车换
                                    购热销商品</em>
                            </div>
                        </div>
                    </div>
                    <div class="support">
                        <div class="summary-wrap">
                            <div class="fl title">
                                <i>支　　持</i>
                            </div>
                            <div class="fl fix-width">
                                <em class="t-gray">以旧换新，闲置手机回收  4G套餐超值抢  礼品购</em>
                            </div>
                        </div>
                        <div class="summary-wrap">
                            <div class="fl title">
                                <i>配 送 至</i>
                            </div>
                            <div class="fl fix-width">
                                <em class="t-gray">满999.00另加20.00元，或满1999.00另加30.00元，或满2999.00另加40.00元，即可在购物车换购热销商品</em>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix choose">
                        <div id="specification" class="summary-wrap clearfix">
                <#assign specificationList=goodsDesc.specificationItems?eval>
                            <#list specificationList as specification>
                            <dl>
                                <dt>
                                    <div class="fl title">
                                        <i>${specification.attributeName}</i>
                                    </div>
                                </dt>
                                <#list specification.attributeValue as attributeValue >
                                <dd><a href="javascript:;" class="{{isSelected('${specification.attributeName}','${attributeValue}')?'selected':''}}"
                                    ng-click="selectSpecification('${specification.attributeName}','${attributeValue}')">${attributeValue}<span title="点击取消选择">&nbsp;</span>
                                </a></dd>
                                </#list>
                            </dl>

                            </#list>

                        </div>









                        <div class="summary-wrap">
                            <div class="fl title">
                                <div class="control-group">
                                    <div class="controls">
                                        <input autocomplete="off" type="text" value="{{num}}" minnum="1" class="itxt" />
                                        <a href="javascript:void(0)" class="increment plus" ng-click="addNum(1)">+</a>
                                        <a href="javascript:void(0)" class="increment mins" ng-click="addNum(-1)">-</a>
                                    </div>
                                </div>
                            </div>
                            <div class="fl">
                                <ul class="btn-choose unstyled">
                                    <li>
                                        <a  target="_blank" class="sui-btn  btn-danger addshopcar" ng-click="addToCart()">加入购物车</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>